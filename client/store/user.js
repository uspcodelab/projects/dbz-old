import axios from 'axios'
import { baseAPIUrl } from '.'

const decodeJWT = token => {
  if (token == '')
    return

  let base64Url = token.split('.')[1]
  let base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}

export const userState = {
  email: '',
  token: '',
  nusp: '',
  nome: '',
  startingYear: '',
}

export const userGetters = {
  isAuthenticated(state) {
    return state.token !== ''
  },

  getToken(state) {
    return state.token
  },

  getEmail(state) {
    return state.email
  },

  getNusp(state) {
    return state.nusp
  },

  getStartingYear(state) {
    return state.startingYear
  },

  getNome(state) {
    return state.nome
  },

  getUUID(state) {
    const decodedToken = decodeJWT(state.token)
    if (decodedToken === undefined) return ''
    return decodeJWT(state.token).id_usuario
  }
}

export const userActions = {
  async getNusp({ commit, getters }) {
    const uuid = getters.getUUID
    const response = await axios.post(`${baseAPIUrl}/rpc/busca_aluno_nusp`, {
      "id_usr": uuid
    })

    commit('setNusp', { nusp: response.data })
  },

  async getNome ({ commit, getters }) {
    const uuid = getters.getUUID
    const response = await axios.post(`${baseAPIUrl}/rpc/busca_nome_usuario`, {
      "id_usr": uuid
    })

    commit('setNome', { nome: response.data })
  },

  async getStartingYear({ commit, getters }) {
    const nusp = getters.getNusp
    const response = await axios.get(`${baseAPIUrl}/alunos?nusp_aluno=eq.${nusp}`)
    const startingYear = response.data[0].turma_ingresso.split('-')[0]
    commit('setStartingYear', { startingYear })
  }
}

export const userMutations = {
  invalidateUser(state) {
    state.email = ''
    state.token = ''
    state.nusp = ''
    state.nome = ''
    state.startingYear = ''
  },

  setEmail(state, { email }) {
    state.email = email
  },

  setToken(state, { token }) {
    state.token = token
  },

  setNusp(state, { nusp }) {
    state.nusp = nusp
  },

  setStartingYear(state, { startingYear }) {
    state.startingYear = startingYear
  },

  setNome(state, { nome }) {
    state.nome = nome
  },
}
