const statuses = ["nulo", "cursada", "planejada", "bloqueada"];
const symbols = ["", "check", "pen", "lock"];
const colors = ["#C0C0C0", "#3F51B5", "#FF9800", "#909090"];

const AVALAIBLE_STATUS = 3

export const visualizationState = {
  status: {}
};

export const visualizationGetters = {
  getStatus(state) {
    return code => {
      return state.status[`${code}`];
    };
  },

  isApproved(state) {
    return code => {
      return state.approvedSubjects.includes(code);
    };
  },

  isBlocked(state) {
    return code => {

      const key = `${code}`

      if (state.preRequisitos[key] === undefined)
        return false


      return state.preRequisitos[key].every(subject => {
        return !state.approvedSubjects.includes(subject)
      })
    }
  }
};

export const visualizationActions = {
  setSubjectStatus({ commit, getters }, { code }) {
    const status = getters.getStatus(code).status;
    const index = (statuses.indexOf(status) + 1) % AVALAIBLE_STATUS;
    const view = getters.getView

    commit("setStatus", {
      code,
      index
    });
  }
};

export const visualizationMutations = {
  invalidateVisualization(state) {
    let newState = Object.assign({}, state.status);

    for (let key in newState) {
      newState[key] = {
        status: statuses[0],
        symbol: symbols[0],
        color: colors[0]
      };
    }

    state.status = newState;
  },

  setStatus(state, { code, index }) {

    let newState = { ...state.status };
    newState[`${code}`] = {
      status: statuses[index],
      symbol: symbols[index],
      color: colors[index]
    };

    state.status = newState;
  },

  setPlannedAsNull(state) {
    let newState = Object.assign({}, state.status);

    for (let key in newState) {
      if (newState[key].status === "planejada") {
        newState[key] = {
          status: statuses[0],
          symbol: symbols[0],
          color: colors[0]
        };
      }
    }

    state.status = newState;
  }
};
