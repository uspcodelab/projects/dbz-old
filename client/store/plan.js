import axios from 'axios'
import { baseAPIUrl } from '.'

export const planState = {
  numPlanNext: 0,
  planning: {},
  planNumber: '',
  planNumbers: [],
  activePlan: [],
}

export const planGetters = {
  isPlanned(state) {
    return code => {
      return state.plannedSubjects.includes(code)
    }
  },

  getPlanNumbers(state) {
    return state.planNumbers
  },

  getNumPlanNext(state) {
    return state.numPlanNext
  },

    getPlanning(state) {
    return state.planning
  },

  getPlanNumber(state) {
    return state.planNumber
  },

  getActivePlan(state) {
    return state.activePlan
  }
}

export const planActions = {
  async getPlans({ commit, getters }) {
    const nusp = getters.getNusp;
    const response = await axios.post(`${baseAPIUrl}/rpc/busca_planos`, { nusp });

    const planNumbers = response.data.map(num => {
      return num.num_plano;
    });

    let max = 0;
    planNumbers.map(num => {
      if (num > max) max = num;
    })
    console.log(max);

    const numPlanNext = max + 1;

    commit('setPlanNumbers', { planNumbers });
    commit('setNumPlanNext', { numPlanNext });
  },

  async getActivePlan({commit, getters}) {
    const response = await axios.post(`${baseAPIUrl}/rpc/busca_plano_numero`, {
      nusp: getters.getNusp,
      n_plano: getters.getPlanNumber
    });

    commit('setActivePlan', { activePlan: response.data })
  },

  addToPlanning({ commit, getters }, {code, counter}) {
    let semNumber = +getters.getStartingYear + Math.ceil(counter/2) - 1;
    semNumber = counter % 2 === 0 ? semNumber + '.2' : semNumber + '.1' ;

    commit('setPlanning', { code, semNumber });
  },

  async commitPlan({ commit, getters, dispatch }) {
    const nusp = getters.getNusp;
    const planning = getters.getPlanning;
    let numPlanNext = getters.getNumPlanNext;

    for (let i in planning) {
      await axios.post(`${baseAPIUrl}/rpc/cria_plano`, {
        nusp,
        cdg_disciplina: i,
        ano_semestre: +planning[i].semNumber,
        num_plano: numPlanNext,
      },
      { headers: {Authorization: `Bearer ${getters.getToken}`} }
      );
    }

    numPlanNext++;
    commit('setNumPlanNext', { numPlanNext });
    commit('cleanPlanning');
  },
}

export const planMutations = {
  invalidatePlan(state) {
    state.planning = {}
    state.numPlanNext = 0
    state.planNumber = ''
    state.planNumbers = []
    state.activePlan = []
  },

  setActivePlan(state, { activePlan }) {
    state.activePlan = activePlan
  },

  setPlanNumber(state, {planNumber}) {
    state.planNumber = planNumber
  },

  setPlanning(state, { code, semNumber }) {
    state.planning[`${code}`] = { semNumber }
  },

  setPlanNumbers(state, { planNumbers }) {
    state.planNumbers = planNumbers;
  },

  setNumPlanNext(state, { numPlanNext }) {
    state.numPlanNext = numPlanNext;
  },

  cleanPlanning(state) {
    state.planning = {};
  },
}
