export const creditsState = {
  cred_obg_approved: 0,
  cred_el_approved: 0,
  cred_livre_approved: 0,
  cred_obg_planned: 0,
  cred_el_planned: 0,
  cred_livre_planned: 0
}

export const creditsGetters = {
  getCredObgApproved(state) {
    return state.cred_obg_approved
  },

  getCredElApproved(state) {
    return state.cred_el_approved
  },

  getCredLivreApproved(state) {
    return state.cred_livre_approved
  },

  getCredObgPlanned(state) {
    return state.cred_obg_planned
  },

  getCredElPlanned(state) {
    return state.cred_el_planned
  },

  getCredLivrePlanned(state) {
    return state.cred_livre_planned
  }
}

export const creditsMutations = {
  invalidateCredits(state) {
    state.cred_obg_approved = 0
    state.cred_el_approved = 0
    state.cred_livre_approved = 0
    state.cred_obg_planned = 0
    state.cred_el_planned = 0
    state.cred_livre_planned = 0
  },

  setPlannedCreditsToZero(state) {
    state.cred_obg_planned = 0
    state.cred_el_planned = 0
    state.cred_livre_planned = 0
  },

  setCredits(state,
    { cred_obg, cred_el, cred_livre }) {
    state[`cred_obg_approved`] = cred_obg
    state[`cred_el_approved`] = cred_el
    state[`cred_livre_approved`] = cred_livre
  },

  addCredits(state, {credits, code, type, name}) {
    state[`cred_${type}_${name}`] += credits
    state[`${name}Subjects`].push(code)
  },

  removeCredits(state, {credits, code, type, name}) {
    state[`cred_${type}_${name}`] -= credits
    state[`${name}Subjects`].splice(state[`${name}Subjects`].indexOf(code), 1)
  }
}
