import axios from 'axios'
import { baseAPIUrl } from '.'

export const subjectsState = {
  approvedSubjects: [],
  approvedSubjectsCounters: {},
  approvedSemester: [],
  plannedSubjects: [],
  blockedSubjects: [],
  plannedSemester: [],
  preRequisitos: {},
}

export const subjectsGetters = {
  getApprovedSubjects(state) {
    return state.approvedSubjects
  },

  getApprovedSemester(state) {
    return state.approvedSemester
  },

  getPlannedSubjects(state) {
    return state.plannedSubjects
  },

  getBlockedSubjects(state) {
    return state.blockedSubjects
  },

  getPlannedSemester(state) {
    return state.plannedSemester
  }
}

export const subjectsActions = {
  async getSubjects({ commit }, {payload, url, setter, name}) {
    const response = await axios.post(`${baseAPIUrl}/rpc/${url}`, payload)

    if (response === undefined) return

    const subjects = response.data.map(obj => {
      return obj['codigo_disciplina']
    })

    let obj = {}
    obj[`${name}`] = subjects

    commit(`${setter}`, obj)
  },

  async setSemester({commit}, { type, name , payload, setter }) {
    const response =
      await this.$axios.$post(
        `${baseAPIUrl}/rpc/disciplinas_${type}_semestre`,
        payload
      )

    let obj = {}
    obj[`${name}Semester`] = response

    commit(`set${setter}Semester`, obj)
  }
}

export const subjectsMutations = {
  invalidateSubjects(state) {
    state.approvedSubjects = []
    state.plannedSubjects = []
    state.blockedSubjects = []
    state.approvedSemester = []
    state.plannedSemester = []
    state.preRequisitos = {}
  },

  setApprovedSubjects(state, { approvedSubjects }) {
    state.approvedSubjects = approvedSubjects
  },

  setPlannedSubjects(state, { plannedSubjects }) {
    state.plannedSubjects = plannedSubjects
  },

  setBlockedSubjects(state, { blockedSubjects }) {
    state.blockedSubjects = blockedSubjects
  },

  setApprovedSemester(state, { approvedSemester }) {
    state.approvedSemester = approvedSemester
  },

  setPlannedSemester(state, { plannedSemester }) {
    state.plannedSemester = plannedSemester
  },

  setPreRequisitos(state, { preRequisitos }) {
    preRequisitos.forEach(({ dsp_codigo, prq_codigo }) => {
      const key = `${dsp_codigo}`

      if (state.preRequisitos[key] === undefined)
        state.preRequisitos[key] = []

      if (!state.preRequisitos[key].includes(prq_codigo))
        state.preRequisitos[key].push(prq_codigo)
    })
  }
}
