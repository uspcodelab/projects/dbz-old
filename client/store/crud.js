export const crudState = {
  activeTrilha: {},
  activeModulo: {},
  activeOptativa: {}
}

export const crudGetters = {
  getActiveTrilha(state) {
    return state.activeTrilha
  },

  getActiveModulo(state) {
    return state.activeModulo
  },

  getActiveOptativa(state) {
    return state.activeOptativa
  }
}

export const crudMutations = {
  invalidateCrud(state) {
    state.activeTrilha = {}
    state.activeModulo = {}
    state.activeOptativa = {}
  },

  setActiveTrilha(state, { trilha }) {
    state.activeTrilha = trilha;
  },

  setActiveModulo(state, { modulo }) {
    state.activeModulo = modulo;
  },

  setActiveOptativa(state, { optativa }) {
    state.activeOptativa = optativa;
  }
}
