export const navigationState = {
  view: 'curso',
  onIndex: true,
  selectedSemester: 'Geral',
  mode: '',
  activeSubject: ''
}

export const navigationGetters = {
  getMode(state) {
    return state.mode
  },

  isOnCourse(state) {
    return state.view === 'curso'
  },

  getView(state) {
    return state.view
  },

  isOnPlan(state) {
    return state.view.includes('plano')
  },

  isOnIndex(state) {
    return state.onIndex
  },

  getSelectedSemester(state) {
    return state.selectedSemester
  },

  getSemesterNumber(state) {
    let sem = state.selectedSemester.slice(0, -1)
    let semNumber = +state.startingYear + Math.floor(+sem/2)
    return +sem % 2 === 0 ? semNumber + '.2' : semNumber + '.1'
  },

  getActiveSubject(state) {
    return state.activeSubject;
  },
}

export const navigationMutations = {
  invalidadeNavigation(state) {
    state.view = 'curso'
    state.onIndex = true
    state.selectedSemester = 'Geral'
    state.mode = ''
    state.activeSubject = ''
  },

  setView(state, { view }) {
    state.view = view
  },

  setOnIndex(state, {onIndex}) {
    state.onIndex = onIndex
  },

  setMode(state, { mode }) {
    state.mode = mode;
  },

  setSelectedSemester(state, { selectedSemester }) {
    state.selectedSemester = selectedSemester;
  },

  setActiveSubject(state, {code}) {
    state.activeSubject = code
  }
}
