import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import axios from "axios";

import { userState, userGetters, userActions, userMutations } from "./user";

import { creditsState, creditsGetters, creditsMutations } from "./credits";

import { planState, planGetters, planActions, planMutations } from "./plan";

import { crudState, crudGetters, crudMutations } from "./crud";

import {
  navigationState,
  navigationGetters,
  navigationMutations
} from "./navigation";

import {
  subjectsState,
  subjectsGetters,
  subjectsActions,
  subjectsMutations
} from "./subjects";

import {
  visualizationState,
  visualizationGetters,
  visualizationActions,
  visualizationMutations
} from "./visualization";

export const baseAPIUrl = "http://localhost:3000";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      ...userState,
      ...creditsState,
      ...navigationState,
      ...planState,
      ...subjectsState,
      ...crudState,
      ...visualizationState
    },

    getters: {
      ...userGetters,
      ...creditsGetters,
      ...navigationGetters,
      ...planGetters,
      ...subjectsGetters,
      ...crudGetters,
      ...visualizationGetters
    },

    actions: {
      ...userActions,
      ...planActions,
      ...subjectsActions,
      ...visualizationActions
    },

    mutations: {
      ...userMutations,
      ...creditsMutations,
      ...navigationMutations,
      ...planMutations,
      ...subjectsMutations,
      ...crudMutations,
      ...visualizationMutations
    }
  });
};
