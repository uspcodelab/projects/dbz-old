# MAC0350-PROJECT

# Integrantes
Leonardo Lana Violin Oliveira - 9793735
Beatriz Figueiredo Marouelli - 9793652

# Link do repositório
https://gitlab.com/leolanavo/MAC0350

## O que faltou
1. Botão para ir para os CRUDS
2. Show dos CRUDS
3. Visualização de plano salvo (front)
4. Disciplinas bloqueadas (front)
5. Salvar disciplinas aprovadas (não sabemos ao certo se isto deveria ser feito
dado que não temos como cadastrar um oferecimento)
