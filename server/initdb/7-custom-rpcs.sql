--- Dado o id_usuario, retornar o numero usp
CREATE OR REPLACE FUNCTION
  public.busca_aluno_nusp(id_usr uuid)
  RETURNS varchar(10)
  LANGUAGE PLPGSQL
AS $$
DECLARE
  nusp varchar(10);
BEGIN
  nusp := (SELECT nusp_administrador
    FROM public.pessoas_geram_usuarios JOIN public.administradores USING (id_pessoa)
    WHERE id_usuario = id_usr);

  IF nusp IS NULL THEN
    nusp := (SELECT nusp_professor
    FROM public.pessoas_geram_usuarios JOIN public.professores USING (id_pessoa)
    WHERE id_usuario = id_usr);
  END IF;

  IF nusp IS NULL THEN
    nusp := (SELECT nusp_aluno
    FROM public.pessoas_geram_usuarios JOIN public.alunos USING (id_pessoa)
    WHERE id_usuario = id_usr);
  END IF;

  RETURN nusp;
END
$$;

--- Dado o id_usuario, retornar o nome de um aluno
CREATE OR REPLACE FUNCTION
  public.busca_nome_usuario(id_usr uuid)
  RETURNS character varying
  LANGUAGE SQL
AS $$
  SELECT nome
    FROM public.pessoas_geram_usuarios JOIN public.pessoas USING (id_pessoa)
    WHERE id_usuario = id_usr;
$$;

--- Busca o codigo dos pais
CREATE OR REPLACE FUNCTION
  public.pre_requisitos_pais (cdg_disciplina varchar(7))
  RETURNS TABLE(codigo_disciplina character(7))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina
  FROM (((SELECT id_disciplina FROM disciplinas WHERE codigo_disciplina=cdg_disciplina) AS id
    JOIN public.prerequisitos USING (id_disciplina)) AS requisitos
      JOIN public.disciplinas ON requisitos.id_prerequisito=public.disciplinas.id_disciplina);
$$;

--- Busca o codigo dos filhos
CREATE OR REPLACE FUNCTION
  public.pre_requisitos_filhos (cdg_disciplina varchar(7))
  RETURNS TABLE(codigo_disciplina character(7))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina
  FROM (SELECT public.prerequisitos.id_disciplina FROM
        ((SELECT id_disciplina FROM disciplinas WHERE codigo_disciplina=cdg_disciplina) AS id
        JOIN public.prerequisitos ON id.id_disciplina=public.prerequisitos.id_prerequisito)) AS requisitos
      JOIN public.disciplinas USING (id_disciplina);
$$;

--- Disciplinas bloqueadas
CREATE OR REPLACE FUNCTION
  public.nome_disciplinas_bloqueadas (nusp_aln varchar(10))
  RETURNS TABLE(codigo_disciplina character(7))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina FROM
    disciplinas JOIN
      (SELECT * FROM
        (SELECT id_disciplina, id_prerequisito FROM prerequisitos) AS preq
        EXCEPT
        (SELECT prerequisitos.id_disciplina, id_prerequisito
          FROM prerequisitos JOIN
            (SELECT id_disciplina
              FROM professores_oferecem_disciplinas
              JOIN (SELECT id_oferecimento FROM
                alunos_cursam_disciplinas
                WHERE nota >= 5.0 AND nusp_aluno=nusp_aln)
                AS ofa USING (id_oferecimento))
            AS dsa
          ON dsa.id_disciplina = prerequisitos.id_prerequisito))
        AS dsb
    USING (id_disciplina);
$$;

--- Dado o codigo de uma disciplina retornar seu tipo
CREATE OR REPLACE FUNCTION
  public.tipo_disciplina(cdg_disciplina varchar(7))
  RETURNS varchar(5)
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var varchar(5);
  obg bigint;
  el boolean;
BEGIN
  return_var = 'obg';

  obg := (SELECT id_disciplina
    FROM (public.disciplinas JOIN public.grade_obrigatoria USING (id_disciplina))
    WHERE codigo_disciplina=cdg_disciplina);

  IF obg IS NULL THEN
    return_var='el';

    el := (SELECT eletiva
            FROM (public.disciplinas JOIN public.grade_optativa USING (id_disciplina))
            WHERE codigo_disciplina=cdg_disciplina);
    IF el IS FALSE THEN
       return_var = 'livre';
    END IF;
  END IF;

  RETURN return_var;
END
$$;

--- Retorna a tabela prerequisito com codigos ao inves de ids
CREATE OR REPLACE FUNCTION
  public.codigo_prerequisito()
  RETURNS TABLE (cdg_disciplina character(7), cdg_prerequisito character(7))
  LANGUAGE SQL
AS $$
  SELECT cdg_disciplina, codigo_disciplina AS cdg_prerequisito
  FROM (public.disciplinas JOIN
          (SELECT codigo_disciplina AS cdg_disciplina, id_prerequisito
            FROM (public.disciplinas JOIN public.prerequisitos USING (id_disciplina)))
          AS codigos
        ON id_disciplina=id_prerequisito);
$$;

CREATE OR REPLACE FUNCTION
  public.modulo_disciplina(id_modulo integer)
  RETURNS TABLE (id_disciplina bigint, codigo_disciplina character(7), creditos_aula integer,
                 creditos_trabalho integer, codigo_modulo varchar(10), nome varchar(280), ano_grade_optativa date)
  LANGUAGE SQL
AS $$

  SELECT id_disciplina, codigo_disciplina, creditos_aula,
          creditos_trabalho, codigo_modulo, nome, ano_grade_optativa
  FROM public.modulos JOIN
       (SELECT id_disciplina, codigo_disciplina, creditos_aula,
               creditos_trabalho, id_modulo, nome, ano_grade_optativa
          FROM (optativas_compoem_modulos JOIN disciplinas USING (id_disciplina)) AS optativas
          WHERE optativas.id_modulo = modulo_disciplina.id_modulo) AS cdg_optativas
       USING (id_modulo);
$$;

CREATE OR REPLACE FUNCTION
  public.grade_obrigatoria()
  RETURNS TABLE (id_disciplina bigint, codigo_disciplina character(7), creditos_aula integer,
                 creditos_trabalho integer)
  LANGUAGE SQL
AS $$
  SELECT id_disciplina, codigo_disciplina, creditos_aula, creditos_trabalho
    FROM grade_obrigatoria JOIN disciplinas USING (id_disciplina)
$$;

--- Disciplinas aprovadas por ano_semestre
CREATE OR REPLACE FUNCTION
  public.disciplinas_aprovadas_semestre(nusp_aln varchar(10), ano_sem numeric(5, 1))
  RETURNS TABLE(codigo_disciplina character(7), nome varchar(280))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina, nome FROM
  ((SELECT * FROM ((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno))
    JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
      WHERE ano_semestre=ano_sem) AS disciplinas_sem
    JOIN public.disciplinas USING (id_disciplina))
  WHERE nota >= 5.0 AND nusp_aluno=nusp_aln;
$$;

--- Disciplinas aprovadas geral
CREATE OR REPLACE FUNCTION
  public.disciplinas_aprovadas(nusp_aln varchar(10))
  RETURNS TABLE(codigo_disciplina character(7), ano_semestre numeric(5,1))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina, ano_semestre FROM
  (((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno))
    JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
    JOIN public.disciplinas USING (id_disciplina))
  WHERE nota >= 5.0 AND nusp_aluno=nusp_aln;
$$;

--- Disciplinas planejadas por semestre
CREATE OR REPLACE FUNCTION
  public.disciplinas_planejadas_semestre(nusp_aln varchar(10), n_plan bigint, ano_sem numeric(5, 1))
  RETURNS TABLE (codigo_disciplina character(7), nome varchar(280))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina, nome AS materias_planejadas FROM
  ((SELECT *
    FROM (public.alunos JOIN public.alunos_planejam_disciplinas USING (nusp_aluno))
      WHERE num_plano = n_plan AND ano_semestre=ano_sem) AS plano
    JOIN public.disciplinas USING (id_disciplina))
  WHERE nusp_aluno=nusp_aln;
$$;

--- Disciplinas planejadas geral
CREATE OR REPLACE FUNCTION
  public.disciplinas_planejadas(nusp_aln varchar(10), n_plan bigint)
  RETURNS TABLE (codigo_disciplina character(7))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina AS materias_planejadas FROM
  ((SELECT *
    FROM (public.alunos JOIN public.alunos_planejam_disciplinas USING (nusp_aluno))
      WHERE num_plano = n_plan) AS plano
    JOIN public.disciplinas USING (id_disciplina))
  WHERE nusp_aluno=nusp_aln;
$$;

--- Créditos obrigatórios feitos
CREATE OR REPLACE FUNCTION
  public.busca_obrigatorios(nusp_aln varchar(10))
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_obg FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno))
    JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_obrigatoria USING (id_disciplina))
  WHERE nota >= 5.0 AND nusp_aluno=nusp_aln) AS obrigatorias;
$$;

--- Créditos eletivos feitos
CREATE OR REPLACE FUNCTION
  public.busca_eletivos(nusp_aln varchar(10))
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_eletivas FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno))
    JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_optativa USING (id_disciplina))
  WHERE nota >= 5.0 AND eletiva = TRUE AND nusp_aluno=nusp_aln) AS eletivas;
$$;

--- Créditos livres feitos
CREATE OR REPLACE FUNCTION
  public.busca_livres(nusp_aln varchar(10))
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_livre FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno))
    JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_optativa USING (id_disciplina))
  WHERE nota >= 5.0 AND eletiva = FALSE AND nusp_aluno=nusp_aln) AS livres;
$$;

--- Créditos obrigatórios planejados
CREATE OR REPLACE FUNCTION
  public.busca_plan_obrigatorios(nusp_aln varchar(10), n_plan bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_obg FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((SELECT *
      FROM (public.alunos JOIN public.alunos_planejam_disciplinas USING (nusp_aluno))
        WHERE num_plano = n_plan) AS plano
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_obrigatoria USING (id_disciplina)))
  WHERE nusp_aluno=nusp_aln) AS obrigatorias;
$$;

--- Créditos eletivos planejados
CREATE OR REPLACE FUNCTION
  public.busca_plan_eletivos(nusp_aln varchar(10), n_plan bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_obg FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((SELECT *
      FROM (public.alunos JOIN public.alunos_planejam_disciplinas USING (nusp_aluno))
        WHERE num_plano = n_plan) AS plano
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_optativa USING (id_disciplina)))
  WHERE nusp_aluno=nusp_aln AND eletiva = TRUE) AS eletivos;
$$;

--- Créditos livres planejados
CREATE OR REPLACE FUNCTION
  public.busca_plan_livres(nusp_aln varchar(10), n_plan bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_obg FROM
  (SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM
  ((((SELECT *
      FROM (public.alunos JOIN public.alunos_planejam_disciplinas USING (nusp_aluno))
        WHERE num_plano = n_plan) AS plano
    JOIN public.disciplinas USING (id_disciplina))
    JOIN public.grade_optativa USING (id_disciplina)))
  WHERE nusp_aluno=nusp_aln AND eletiva = FALSE) AS livres;
$$;

CREATE OR REPLACE FUNCTION
  public.cria_plano(nusp varchar(10), cdg_disciplina varchar(7),
                    ano_semestre numeric(5,1), num_plano bigint)
  RETURNS VOID
  LANGUAGE PLPGSQL
AS $$
DECLARE
  id_pes bigint;
  id_dscp bigint;
BEGIN
  id_pes := (SELECT id_pessoa
             FROM public.alunos
             WHERE nusp_aluno=nusp);

  id_dscp := (SELECT id_disciplina F
              FROM public.disciplinas
              WHERE codigo_disciplina=cdg_disciplina);

  INSERT INTO public.alunos_planejam_disciplinas
              (num_plano, id_pessoa, nusp_aluno, id_disciplina,
               ano_semestre)
    VALUES(num_plano, id_pes, nusp, id_dscp, ano_semestre);
END
$$;

CREATE OR REPLACE FUNCTION
  public.busca_planos(nusp varchar(10))
  RETURNS TABLE (num_plano bigint)
  LANGUAGE SQL
AS $$
  SELECT DISTINCT num_plano
    FROM public.alunos_planejam_disciplinas
    WHERE nusp_aluno=nusp;
$$;

CREATE OR REPLACE FUNCTION
  public.busca_plano_numero(nusp varchar(10), n_plano bigint)
  RETURNS TABLE(codigo_disciplina character(7), ano_semestre numeric(5,1))
  LANGUAGE SQL
AS $$
  SELECT codigo_disciplina, ano_semestre
    FROM (public.disciplinas JOIN
          (SELECT id_disciplina, ano_semestre
            FROM public.alunos_planejam_disciplinas
            WHERE nusp_aluno=nusp AND num_plano=n_plano)
          AS plano
          USING (id_disciplina));
$$;

--- Pre-requisitos com código
CREATE OR REPLACE FUNCTION
  public.busca_pre_requisitos()
  RETURNS TABLE(dsp_codigo character(7), prq_codigo character(7))
  LANGUAGE SQL
AS $$
  SELECT
    middle.codigo_disciplina AS dsp_codigo,
    disciplinas.codigo_disciplina AS prq_codigo
    FROM disciplinas JOIN (
      SELECT codigo_disciplina, id_prerequisito
        FROM prerequisitos
        JOIN disciplinas
        USING (id_disciplina)
      ) AS middle ON
      middle.id_prerequisito = disciplinas.id_disciplina;
$$;

-----------------------------------------
--- CRUD
-----------------------------------------

--- Trilhas

CREATE OR REPLACE FUNCTION
  public.insere_trilha(nusp varchar(10), codigo_trilha varchar(10),
                       nome varchar(100), descricao varchar(280),
                       minimo_disciplinas integer, minimo_modulos integer)
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    INSERT INTO public.trilhas (codigo_trilha, nome,
                                descricao, minimo_disciplinas,
                                minimo_modulos)
    VALUES (codigo_trilha, nome, descricao,
            minimo_disciplinas, minimo_modulos);
    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

CREATE OR REPLACE FUNCTION
  public.deleta_trilha(nusp varchar(10), cdg_trilha varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    DELETE FROM public.trilhas WHERE codigo_trilha=cdg_trilha;
    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

CREATE OR REPLACE FUNCTION
  public.edita_trilha(nusp varchar(10), cdg_trilha varchar(10),
                      up_cdg_trilha varchar(10), up_nome varchar(100),
                      up_descricao varchar(280), up_min_dscp integer,
                      up_min_mod integer)
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN

    IF up_nome IS NOT NULL THEN
      UPDATE public.trilhas SET nome=up_nome WHERE codigo_trilha=cdg_trilha;
    END IF;

    IF up_descricao IS NOT NULL THEN
      UPDATE public.trilhas SET descricao=up_descricao WHERE codigo_trilha=cdg_trilha;
    END IF;

    IF up_min_dscp IS NOT NULL THEN
      UPDATE public.trilhas SET minimo_disciplinas=up_min_dscp WHERE codigo_trilha=cdg_trilha;
    END IF;

    IF up_min_mod IS NOT NULL THEN
      UPDATE public.trilhas SET minimo_modulos=up_min_mod WHERE codigo_trilha=cdg_trilha;
    END IF;

    IF up_cdg_trilha IS NOT NULL THEN
      UPDATE public.trilhas SET codigo_trilha=up_cdg_trilha WHERE codigo_trilha=cdg_trilha;
    END IF;

    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

--- Modulos
CREATE OR REPLACE FUNCTION
  public.insere_modulo(nusp varchar(10), codigo_modulo varchar(10),
                       minimo_disciplinas integer, cdg_trilha varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
  id_trilha_table bigint;
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    id_trilha_table := (SELECT id_trilha
                    FROM public.trilhas
                    WHERE codigo_trilha=cdg_trilha);

    INSERT INTO public.modulos (codigo_modulo, minimo_disciplinas,
                                id_trilha)
    VALUES (codigo_modulo, minimo_disciplinas,
            id_trilha_table);
    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

CREATE OR REPLACE FUNCTION
  public.deleta_modulo(nusp varchar(10), cdg_modulo varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    DELETE FROM public.modulos WHERE codigo_modulo=cdg_modulo;
    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

CREATE OR REPLACE FUNCTION
  public.edita_modulo(nusp varchar(10), cdg_modulo varchar(10),
                      up_cdg_modulo varchar(10), up_min_dscp integer,
                      up_cdg_trilha varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  return_var boolean;
  nusp_auth varchar(10);
  id_trl bigint;
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN

    IF up_min_dscp IS NOT NULL THEN
      UPDATE public.modulos
        SET minimo_disciplinas=up_min_dscp
        WHERE codigo_modulo=cdg_modulo;
    END IF;

    IF up_cdg_trilha IS NOT NULL THEN
      id_trl := (SELECT id_trilha
                    FROM public.trilhas
                    WHERE codigo_trilha=cdg_trilha);

      UPDATE public.modulos
        SET id_trilha=id_trl
        WHERE codigo_modulo=cdg_modulo;
    END IF;

    IF up_cdg_modulo IS NOT NULL THEN
      UPDATE public.modulos
        SET codigo_modulo=up_cdg_modulo
        WHERE codigo_modulo=cdg_modulo;
    END IF;

    return_var = TRUE;
  END IF;

  RETURN return_var;
END
$$;

---Optativas----------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION
  public.insere_optativa_modulo(nusp varchar(10), cdg_disciplina varchar(7),
                                ano_grade_optativa date, cdg_modulo varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  nusp_auth varchar(10);
  id_dscp bigint;
  id_mod bigint;
  return_var boolean;
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=cdg_disciplina);

    id_mod := (SELECT id_modulo
                FROM public.modulos
                WHERE codigo_modulo=cdg_modulo);

    INSERT INTO public.optativas_compoem_modulos (id_disciplina,
                                                  ano_grade_optativa,
                                                  id_modulo)
      VALUES (id_dscp, ano_grade_optativa,
              id_mod);
    return_var = TRUE;
  END IF;
  RETURN return_var;
END
$$;

CREATE OR REPLACE FUNCTION
  public.deleta_optativa_modulo(nusp varchar(10), cdg_disciplina varchar(7),
                                ano_grd_optativa date, cdg_modulo varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  nusp_auth varchar(10);
  id_dscp bigint;
  id_mod bigint;
  return_var boolean;
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN
    id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=cdg_disciplina);

    id_mod := (SELECT id_modulo
                FROM public.modulos
                WHERE codigo_modulo=cdg_modulo);

    DELETE FROM public.optativas_compoem_modulos
      WHERE id_disciplina = id_dscp AND
            ano_grade_optativa = ano_grd_optativa AND
            id_modulo = id_mod;
    return_var = TRUE;
  END IF;
  RETURN return_var;
END
$$;


CREATE OR REPLACE FUNCTION
  public.edita_optativa_modulo(nusp varchar(10),
    cdg_dscp varchar(7), ano_grd_optativa date, cdg_mod varchar(10),
    up_cdg_dscp varchar(7), up_ano_grd_optativa date, up_cdg_mod varchar(10))
  RETURNS boolean
  LANGUAGE PLPGSQL
AS $$
DECLARE
  nusp_auth varchar(10);
  id_dscp bigint;
  id_mod bigint;
  up_id_dscp bigint;
  up_id_mod bigint;
  return_var boolean;
BEGIN
  return_var = FALSE;
  nusp_auth := (SELECT nusp_administrador
                  FROM public.administradores
                  WHERE nusp_administrador=nusp);

  IF nusp_auth IS NOT NULL THEN

    IF up_cdg_dscp IS NOT NULL THEN
      id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=cdg_dscp);

      up_id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=up_cdg_dscp);

      id_mod := (SELECT id_modulo
                 FROM public.modulos
                 WHERE codigo_modulo=cdg_mod);

      UPDATE public.optativas_compoem_modulos
        SET id_disciplina=up_id_dscp
        WHERE id_disciplina=id_dscp AND
              ano_grade_optativa=ano_grd_optativa AND
              id_modulo=id_mod;

      cdg_dscp = up_cdg_dscp;
    END IF;

    IF up_ano_grd_optativa IS NOT NULL THEN
      id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=cdg_dscp);

      id_mod := (SELECT id_modulo
                 FROM public.modulos
                 WHERE codigo_modulo=cdg_mod);

      UPDATE public.optativas_compoem_modulos
        SET ano_grade_optativa=up_ano_grd_optativa
        WHERE id_disciplina=id_dscp AND
              ano_grade_optativa=ano_grd_optativa AND
              id_modulo=id_mod;

      ano_grd_optativa=up_ano_grd_optativa;
    END IF;

    IF up_cdg_modulo IS NOT NULL THEN
      id_dscp := (SELECT id_disciplina
                  FROM public.disciplinas
                  WHERE codigo_disciplina=cdg_dscp);

      up_id_mod := (SELECT id_modulo
                  FROM public.modulos
                  WHERE codigo_modulo=up_cdg_mod);

      id_mod := (SELECT id_modulo
                 FROM public.modulos
                 WHERE codigo_modulo=cdg_mod);

      UPDATE public.optativas_compoem_modulos
        SET id_modulo=up_id_mod
        WHERE id_disciplina=id_dscp AND
              ano_grade_optativa=ano_grd_optativa AND
              id_modulo=id_mod;
    END IF;

    return_var = TRUE;
  END IF;
  RETURN return_var;
END
$$;
