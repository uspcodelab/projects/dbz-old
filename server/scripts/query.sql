--- Disciplinas aprovadas
SELECT codigo_disciplina AS materias_aprovadas_renato FROM 
(((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
WHERE nota >= 5.0 AND nusp_aluno='0000002';

--- Disciplinas bloqueadas
SELECT id_disciplina FROM
(SELECT id_disciplina, id_prerequisito FROM public.prerequisitos
EXCEPT
SELECT id_disciplina, id_prerequisito FROM (SELECT id_disciplina FROM 
(((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
  WHERE nota >= 5.0 AND nusp_aluno='0000002') AS ma JOIN public.prerequisitos USING (id_disciplina)) AS ma;

--- Disciplinas aprovadas no semestre 2018.1
SELECT codigo_disciplina AS materias_aprovadas FROM 
(((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
WHERE nota >= 5.0 AND ano_semestre=2017.1 AND nusp_aluno='0000002';

--- Créditos obrigatórios feitos
SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_obg FROM
(SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM 
((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
  JOIN public.grade_obrigatoria USING (id_disciplina))
WHERE nota >= 5.0 AND nusp_aluno='0000002') AS obrigatorias;

--- Créditos eletivos
SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_eletivas FROM
(SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM 
((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
  JOIN public.grade_optativa USING (id_disciplina))
WHERE nota >= 5.0 AND eletiva = TRUE AND nusp_aluno='0000002') AS eletivas;

--- Créditos livres
SELECT COALESCE(sum_aula, 0) + COALESCE(sum_trab, 0) AS cred_livre FROM
(SELECT SUM(creditos_aula) AS sum_aula, SUM(creditos_trabalho) AS sum_trab FROM 
((((public.alunos JOIN public.alunos_cursam_disciplinas USING (nusp_aluno)) 
  JOIN public.professores_oferecem_disciplinas USING (id_oferecimento))
  JOIN public.disciplinas USING (id_disciplina)) 
  JOIN public.grade_optativa USING (id_disciplina))
  WHERE nota >= 5.0 AND eletiva = FALSE AND nusp_aluno='0000002') AS livres;